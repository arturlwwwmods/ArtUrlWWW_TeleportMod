from __future__ import absolute_import

from requests.packages.urllib3.packages import ssl_match_hostname

__all__ = ('ssl_match_hostname', )

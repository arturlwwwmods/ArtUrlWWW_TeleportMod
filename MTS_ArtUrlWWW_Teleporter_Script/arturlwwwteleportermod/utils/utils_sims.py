import enum
import placement
import routing
import services
import sims4.commands
import sims4.math
from objects import ALL_HIDDEN_REASONS
from sims.sim import Sim
from sims.sim_info import SimInfo
from sims4.resources import Types

from arturlwwwteleportermod.utils.exceptions_handler import exception_catcher


@exception_catcher()
def get_sim_instance(sim_instance_or_sim_id_or_sim_info):
    if sim_instance_or_sim_id_or_sim_info is None:
        return
    if isinstance(sim_instance_or_sim_id_or_sim_info, Sim):
        return sim_instance_or_sim_id_or_sim_info
    if isinstance(sim_instance_or_sim_id_or_sim_info, SimInfo):
        return sim_instance_or_sim_id_or_sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)
    if isinstance(sim_instance_or_sim_id_or_sim_info, int):
        return get_sim_instance(services.sim_info_manager().get(sim_instance_or_sim_id_or_sim_info))
    return


@exception_catcher()
def find_good_location_for_sim(sim_identifier):
    sim = get_sim_instance(sim_identifier)  # type: Sim
    (location, on_surface) = sim.get_location_on_nearest_surface_below()
    ignored_object_ids = {sim.sim_id}
    ignored_object_ids.update(child.id for child in sim.children_recursive_gen())
    parent_object = sim.parent_object()
    if parent_object is not None:
        ignored_object_ids.add(parent_object.id)
    search_flags = placement.FGLSearchFlagsDefault | placement.FGLSearchFlag.SHOULD_TEST_BUILDBUY \
                   | placement.FGLSearchFlag.USE_SIM_FOOTPRINT | placement.FGLSearchFlag.SHOULD_TEST_ROUTING
    starting_location = placement.create_starting_location(location=location)
    fgl_context = placement.FindGoodLocationContext(starting_location, ignored_object_ids=ignored_object_ids,
                                                    additional_avoid_sim_radius=routing.get_sim_extra_clearance_distance(),
                                                    search_flags=search_flags, routing_context=sim.routing_context)
    (trans, orient) = placement.find_good_location(fgl_context)
    if trans is None or orient is None:
        sim.fgl_reset_to_landing_strip()
        return
    new_transform = sims4.math.Transform(trans, orient)
    return location.clone(transform=new_transform)


class Age(enum.Int):
    __qualname__ = 'Age'
    BABY = 1
    TODDLER = 2
    CHILD = 4
    TEEN = 8
    YOUNGADULT = 16
    ADULT = 32
    ELDER = 64


@exception_catcher()
def do_ageup_cleanup(sim_info):
    from random import choice, seed
    sim_info.career_tracker.remove_invalid_careers()
    trait_tracker = sim_info.trait_tracker
    trait_tracker.remove_invalid_traits()
    if sim_info.is_npc:
        seed(None)
        if sim_info.is_child or sim_info.is_teen:
            available_aspirations = []
            aspiration_track_manager = services.get_instance_manager(Types.ASPIRATION_TRACK)
            for aspiration_track in aspiration_track_manager.types.values():
                if aspiration_track.is_child_aspiration_track:
                    if sim_info.is_child:
                        available_aspirations.append(aspiration_track)
                        if sim_info.is_teen:
                            available_aspirations.append(aspiration_track)
                elif sim_info.is_teen:
                    available_aspirations.append(aspiration_track)
                    continue

            sim_info.primary_aspiration = choice(available_aspirations)
        empty_trait_slots = trait_tracker.empty_slot_number
        if empty_trait_slots:
            available_traits = [trait for trait in services.trait_manager().types.values() if
                                trait.is_personality_trait]
            while empty_trait_slots > 0 and available_traits:
                trait = choice(available_traits)
                available_traits.remove(trait)
                if not trait_tracker.can_add_trait(trait):
                    continue
                if sim_info.add_trait(trait):
                    empty_trait_slots -= 1
                    continue

    else:
        if sim_info.whim_tracker is not None:
            sim_info.whim_tracker.validate_goals()
        sim_info._apply_life_skill_traits()
        return


@exception_catcher()
def sim_change_age(sim_info, to_age):
    if sim_info is None:
        return
    previous_age = sim_info.age
    sim_info.relationship_tracker.update_bits_on_age_up(previous_age)
    previous_trait_guid = None
    if previous_age < Age.CHILD and to_age > previous_age:
        for trait in sim_info.trait_tracker.personality_traits:
            previous_trait_guid = trait.guid64

        if previous_trait_guid:
            from arturlwwwteleportermod.utils.utils_traits import remove_trait_from_sim
            remove_trait_from_sim(sim_info, previous_trait_guid)
    previous_skills = {}
    for skill in sim_info.all_skills():
        if skill.age_up_skill_transition_data is not None:
            previous_skills[skill] = skill.get_user_value()
            continue

    sim_info.apply_age(to_age)
    sim_info.reset_age_progress()
    do_ageup_cleanup(sim_info)
    if previous_age < sim_info.age and not sim_info.is_npc:
        from arturlwwwteleportermod.utils.interface_utils import show_sim_age_up_dialog
        show_sim_age_up_dialog(sim_info, previous_skills=previous_skills,
                               previous_trait_guid=previous_trait_guid)


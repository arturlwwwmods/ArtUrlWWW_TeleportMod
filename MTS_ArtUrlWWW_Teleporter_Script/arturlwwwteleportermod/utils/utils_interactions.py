import services
from event_testing.results import EnqueueResult
from interactions.context import InteractionContext, QueueInsertStrategy
from interactions.priority import Priority
from sims4.resources import Types


def run_interaction(active_sim, target_sim, interaction_id):
    interaction_context = InteractionContext.SOURCE_SCRIPT_WITH_USER_INTENT
    priority = Priority.Critical
    run_priority = Priority.Critical
    insert_strategy = QueueInsertStrategy.NEXT
    must_run_next = True

    si_affordance_instance = services.get_instance_manager(Types.INTERACTION).get(int(interaction_id))

    context = InteractionContext(active_sim, interaction_context, priority, run_priority=run_priority,
                                 insert_strategy=insert_strategy, must_run_next=must_run_next)

    result = active_sim.push_super_affordance(si_affordance_instance, target_sim, context,
                                              picked_object=target_sim)  # type: EnqueueResult
    return result

import services
from sims4.resources import Types, CompoundTypes
from sims4.tuning.instances import HashedTunedInstanceMetaclass
from sims4.tuning.tunable import TunableResourceKey


class WickedWoohooIconObject(metaclass=HashedTunedInstanceMetaclass,
                             manager=services.get_instance_manager(Types.SNIPPET)):
    __qualname__ = 'WickedWoohooIconObject'
    INSTANCE_TUNABLES = {'icon': TunableResourceKey(resource_types=CompoundTypes.IMAGE)}

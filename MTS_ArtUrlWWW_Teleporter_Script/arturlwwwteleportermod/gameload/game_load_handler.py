import services
from clock import ClockSpeedMode
from sims.sim import Sim
from sims4.localization import LocalizationHelperTuning

import requests
from ehp import *
from arturlwwwteleportermod.MTS_ArtUrlWWW_Teleporter_Instances import MTS_ArtUrlWWW_Teleporter_instance_ids
from arturlwwwteleportermod.VersionInfo import GLOBAL_VERSION, RELEASE_BUILD_NUMBER
from arturlwwwteleportermod.gameload import injector
from arturlwwwteleportermod.gameload.global_manager import register_on_tick_game_function, unregister_on_tick_game_function
from arturlwwwteleportermod.utils.MyLogger import MyLogger
from arturlwwwteleportermod.utils.exceptions_handler import exception_catcher
from arturlwwwteleportermod.utils.interface_utils import display_question_dialog
from arturlwwwteleportermod.utils.interface_utils import show_notification
from arturlwwwteleportermod.utils.string_utils import get_localized_string

HAS_DISPLAYED_HELLO_MSG = False


@exception_catcher()
@injector.inject_to(Sim, 'on_add')
def pregnancymod_add_super_affordances(original, self):
    original(self)
    sa_list = []

    with MyLogger(fileName="ArtUrlWWW_MegaMod_Script_added_affordances.txt", writeMethod="wt") as f:
        f.write("Starting..." + "\n")

        affordance_manager = services.affordance_manager()
        for sa_id in MTS_ArtUrlWWW_Teleporter_instance_ids:
            tuning_class = affordance_manager.get(sa_id)
            f.write("Trying " + str(sa_id) + "\n")
            if not tuning_class is None:
                sa_list.append(tuning_class)
                f.write("Added " + str(sa_id) + "\n")
            else:
                f.write("Can't add " + str(sa_id) + " !!!\n")
        self._super_affordances = self._super_affordances + tuple(sa_list)

        # items = VariableHandler.registeredModules()
        # l = MyLogger.get_main_logger()  # type: MyLogger
        # for s in items:
        #     l.writeLine(s)


@exception_catcher()
class FileLogHandler:
    def __init__(self, name, flags="wt", *args, **kwargs):
        self.file = open(name, flags, *args, **kwargs)

    def write(self, string):
        self.file.write(string)
        self.flush()  # maybe overused but ensures file is updated regularly

    def close(self):
        self.file.close()

    def flush(self):
        self.file.flush()


@exception_catcher()
@register_on_tick_game_function()
def _display_hello_notification_on_tick(ticks):
    global HAS_DISPLAYED_HELLO_MSG
    if HAS_DISPLAYED_HELLO_MSG is False:
        HAS_DISPLAYED_HELLO_MSG = True
        checknewversiononartfunpw()
        checknewversiononmodthesims()
        unregister_on_tick_game_function('_display_hello_notification_on_tick')


@exception_catcher()
def checknewversiononartfunpw():
    r = requests.get('http://artfun.pw/mods/teleport/?v=2')

    if r.status_code == 200:
        modVersion = None
        modVersionDate = None
        html = Html()
        dom = html.feed(r.text)
        for ind in dom.find('span', ('class', 'versionDiv')):
            modVersion = ind.text().strip()

        for ind in dom.find('span', ('class', 'releaseDateDiv')):
            modVersionDate = ind.text().strip()

        if modVersion is not None:
            modVersionNumbers = modVersion.split(".")
            if int(modVersionNumbers[0]) > int(GLOBAL_VERSION):
                _show_need_update_message_universal(modVersion, modVersionDate, 'http://artfun.pw/mods/teleport/')
            else:
                if int(modVersionNumbers[0]) == int(GLOBAL_VERSION):
                    if int(modVersionNumbers[1]) > int(RELEASE_BUILD_NUMBER):
                        _show_need_update_message_universal(modVersion, modVersionDate, 'http://artfun.pw/mods/teleport/')


@exception_catcher()
def _show_need_update_message_universal(modVersion, modVersionDate, urlToOpen):

    localized_title = LocalizationHelperTuning.get_raw_text("!!!MTS_ArtUrlWWW_Teleporter!!!");
    localized_text = LocalizationHelperTuning.get_raw_text(
        "A new version "+str(modVersion)+" of ArtUrlWWW Teleporter from "+str(modVersionDate)+" is available! \n\nDo you want to visit ArtUrlWWW Teleporter page to download file?\n\nThis will open your web browser!");

    def question_callback(dialog):
        if dialog.accepted:
            import webbrowser
            webbrowser.open(urlToOpen)
            services.game_clock_service().set_clock_speed(ClockSpeedMode.PAUSED)

    display_question_dialog(text=localized_text, callback=question_callback)

    show_notification(text=localized_text)

    # text = get_localized_string(object_value=0x82A826BE, tokens=(modVersion, modVersionDate), )
    # display_question_dialog(text=text, callback=question_callback)
    #
    # text = get_localized_string(object_value=0x9B623C87, tokens=(modVersion, modVersionDate), )
    # show_notification(text=text)



@exception_catcher()
def checknewversiononmodthesims():
    r = requests.get('http://www.modthesims.info/download.php?t=600424&v=1')

    if r.status_code == 200:
        modVersion = None
        html = Html()
        dom = html.feed(r.text)
        # dom = html.feed(doc)
        # for ind in dom.find('title'):
        for ind in dom.find('div', ('class', 'pull-left')):
            htmlText = ind.text()
            if htmlText.find("by ArtUrlWWW :") > -1:
                modVersion = htmlText[(htmlText.find("by ArtUrlWWW :") + 14):].strip()

        if modVersion is not None:
            modVersion = modVersion.split(";")
            modVersionNumbers = modVersion[0].split(".")
            if int(modVersionNumbers[0]) > int(GLOBAL_VERSION):
                _show_need_update_message_universal(modVersion, modVersionNumbers, 'http://www.modthesims.info/download.php?t=600424')
            else:
                if int(modVersionNumbers[0]) == int(GLOBAL_VERSION):
                    if int(modVersionNumbers[1]) > int(RELEASE_BUILD_NUMBER):
                        _show_need_update_message_universal(modVersion, modVersionNumbers, 'http://www.modthesims.info/download.php?t=600424')


@exception_catcher()
def _show_need_update_message(modVersion, modVersionNumbers):
    localized_title = LocalizationHelperTuning.get_raw_text("!!!ArtUrlWWW Teleporter!!!");
    localized_text = LocalizationHelperTuning.get_raw_text(
        "A new version {0.String} of ArtUrlWWW Teleporter from {1.String} is available! ");

    def question_callback(dialog):
        if dialog.accepted:
            import webbrowser
            webbrowser.open('http://www.modthesims.info/download.php?t=600424')
            services.game_clock_service().set_clock_speed(ClockSpeedMode.PAUSED)

    text = get_localized_string(object_value=0x82A826BE, tokens=(modVersion[0], modVersion[1]), )
    display_question_dialog(text=text, callback=question_callback)

    text = get_localized_string(object_value=0x9B623C87, tokens=(modVersion[0], modVersion[1]), )
    show_notification(text=text)

    # visual_type = UiDialogNotification.UiDialogNotificationVisualType.INFORMATION
    # urgency = UiDialogNotification.UiDialogNotificationUrgency.DEFAULT
    # client = services.client_manager().get_first_client()
    # # Prepare and show the notification
    # notification = UiDialogNotification.TunableFactory().default(client.active_sim,
    #                                                              text=lambda **_: localized_text,
    #                                                              title=lambda **_: localized_title,
    #                                                              visual_type=visual_type, urgency=urgency)
    # notification.show_dialog()

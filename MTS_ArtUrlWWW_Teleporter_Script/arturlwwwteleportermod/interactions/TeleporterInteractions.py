from event_testing.results import TestResult
from interactions.base.immediate_interaction import ImmediateSuperInteraction
from sims.sim import Sim
from sims.sim_info import SimInfo

from arturlwwwteleportermod.Teleporter import teleportSelectedSimNearActiveSim, MovePickedSimsToCurrentHH_F
from arturlwwwteleportermod.utils.exceptions_handler import exception_catcher


class TeleportSelectedSimNearActiveSim(ImmediateSuperInteraction):
    __qualname__ = 'TeleportSelectedSimNearActiveSim'

    target_sim_info = None

    @classmethod
    def _test(cls, target, context, **interaction_parameters):
        if isinstance(target, Sim):
            cls.target_sim_info = target.sim_info  # type: SimInfo
        else:
            cls.target_sim_info = target  # type: SimInfo

        return TestResult.TRUE

    @exception_catcher()
    def _run_interaction_gen(self, timeline):
        if self.target_sim_info is None:
            sim_info = self.sim.sim_info  # type: SimInfo
            sim = self.sim  # type: Sim
        else:
            sim_info = self.target_sim_info

        teleportSelectedSimNearActiveSim()


class MovePickedSimsToCurrentHH(ImmediateSuperInteraction):
    __qualname__ = 'MovePickedSimsToCurrentHH'

    target_sim_info = None

    @classmethod
    def _test(cls, target, context, **interaction_parameters):
        if isinstance(target, Sim):
            cls.target_sim_info = target.sim_info  # type: SimInfo
        else:
            cls.target_sim_info = target  # type: SimInfo

        return TestResult.TRUE

    @exception_catcher()
    def _run_interaction_gen(self, timeline):
        if self.target_sim_info is None:
            sim_info = self.sim.sim_info  # type: SimInfo
            sim = self.sim  # type: Sim
        else:
            sim_info = self.target_sim_info

        MovePickedSimsToCurrentHH_F(sim_info)
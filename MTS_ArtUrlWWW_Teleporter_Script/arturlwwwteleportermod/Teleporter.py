import threading
import traceback

import services
import sims4.commands
import sims4.math
from objects import ALL_HIDDEN_REASONS, HiddenReasonFlag
from objects.object_enums import ResetReason
from sims.outfits.outfit_enums import OutfitCategory
from sims.sim import Sim
from sims.sim_info import SimInfo
from sims.sim_info_manager import SimInfoManager
from sims.sim_spawner import SimSpawner
from ui.ui_dialog_picker import SimPickerRow, UiSimPicker

from arturlwwwteleportermod.utils.MyLogger import MyLogger
from arturlwwwteleportermod.utils.exceptions_handler import exception_catcher
from arturlwwwteleportermod.utils.interface_utils import show_notification
from arturlwwwteleportermod.utils.string_utils import get_localized_string
from arturlwwwteleportermod.utils.utils_interactions import run_interaction


#######################################

def set_zone_on_spawn_my(sim_info):
    current_zone = services.current_zone()
    current_zone_id = current_zone.id
    # if sim_info.is_npc and (
    #                         sim_info._serialization_option == SimSerializationOption.UNDECLARED or
    #                         sim_info._serialization_option == SimSerializationOption.LOT and
    #                         sim_info._zone_id != current_zone_id or
    #                         sim_info._serialization_option == SimSerializationOption.OPEN_STREETS and
    #                         sim_info.world_id != current_zone.open_street_id):
    sim_info.set_current_outfit((OutfitCategory.EVERYDAY, 0))
    # if sim_info._zone_id != current_zone_id:
    sim_info._prespawn_zone_id = sim_info._zone_id
    sim_info._zone_id = current_zone_id
    sim_info.world_id = current_zone.open_street_id


spawnTimerSuccess = 0


def spawnTimer(sim_id, location, active_sim):
    global spawnTimerSuccess

    logger = MyLogger.get_main_logger()  # type: MyLogger

    try:
        picked_sim_info = services.sim_info_manager().get(sim_id)  # type: SimInfo

        sim = picked_sim_info.get_sim_instance()
        sim.location = location

        set_zone_on_spawn_my(picked_sim_info)
        services.sim_info_manager().add_sim_info_if_not_in_manager(picked_sim_info)

        ######################################
        picked_sim = picked_sim_info.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS)  # type: Sim

        picked_sim_info.commodity_tracker.set_all_commodities_to_max(visible_only=True)
        picked_sim_info.singed = False

        picked_sim.commodity_tracker.update_all_commodities()

        picked_sim.show(HiddenReasonFlag.RABBIT_HOLE)
        picked_sim.reset(ResetReason.RESET_EXPECTED, None, 'Teleporting')
        # sim.location = location

        role_tracker = picked_sim.autonomy_component._role_tracker
        role_tracker.reset()
        situation_manager = services.get_zone_situation_manager()
        situation_manager.create_visit_situation(picked_sim)

        guest_role = services.role_state_manager().get(15862)
        picked_sim.add_role(guest_role)
        ######################################

        r = run_interaction(sim, active_sim, 124984)

        spawnTimerSuccess = 1;

    except Exception as e:
        logger.writeLine(str(e))
        traceback.print_exc(file=logger.fileObj)

    if spawnTimerSuccess < 1:
        threading.Timer(1.0, spawnTimer, [sim_id, location, active_sim]).start()


def teleportSelectedSimNearActiveSim():
    client = services.client_manager().get_first_client()

    def get_inputs_callback(dialog):
        global spawnTimerSuccess
        if not dialog.accepted:
            # output("Dialog was closed/cancelled")
            return
        # output("Dialog was accepted")

        client = services.client_manager().get_first_client()

        active_sim = client.active_sim  # type: Sim
        active_sim_info = client.active_sim.sim_info  # type: SimInfo
        # active_sim_info_location_near = find_good_location_for_sim(active_sim_info)

        for sim_id in dialog.get_result_tags():
            picked_sim_info = services.sim_info_manager().get(sim_id)

            (location, on_surface) = active_sim.get_location_on_nearest_surface_below()
            # picked_sim_info.location = location

            logger = MyLogger.get_main_logger()  # type: MyLogger

            try:
                set_zone_on_spawn_my(picked_sim_info)
                SimSpawner.load_sim(sim_id=picked_sim_info.sim_id, startup_location=location)

                spawnTimerSuccess = 0
                spawnTimer(sim_id=picked_sim_info.sim_id, location=location, active_sim=active_sim)

                # //////////////////////////////////////

            except Exception as e:
                logger.writeLine(str(e))
                traceback.print_exc(file=logger.fileObj)
                    
            result = get_localized_string(object_value=0xE4E0BE53) #Can't spawn here. Try again here or in any other location of active sim.
            title = get_localized_string(object_value=0xC9176435) #Teleporter
            show_notification(result, title=title, sim_info=active_sim_info)
            continue

    localized_title = lambda **_: get_localized_string(object_value=2303243092)  # Select sim
    localized_text = lambda **_: get_localized_string(
        object_value=0xD05DE8BF)  # Select sim, that will be teleported near active sim

    max_selectable_immutable = sims4.collections.make_immutable_slots_class(
        set(['multi_select', 'number_selectable', 'max_type']))
    max_selectable = max_selectable_immutable({'multi_select': False, 'number_selectable': 1, 'max_type': 1})
    dialog = UiSimPicker.TunableFactory().default(client.active_sim, text=localized_text, title=localized_title,
                                                  max_selectable=max_selectable, min_selectable=1,
                                                  should_show_names=True,
                                                  hide_row_description=False)

    simInfoManager = services.sim_info_manager()  # type: SimInfoManager
    for sim_info in simInfoManager.get_all():  # type: SimInfo
        dialog.add_row(SimPickerRow(sim_info.sim_id, False, tag=sim_info.sim_id))

    dialog.add_listener(get_inputs_callback)
    dialog.show_dialog(icon_override=(None, sim_info))


@exception_catcher()
def MovePickedSimsToCurrentHH_F(sim_info_inp=None):

    #######################################
    def get_inputs_callback(dialog):
        if not dialog.accepted:
            return

        if sim_info_inp is None:
            client = services.client_manager().get_first_client()
            active_sim_info = client.active_sim.sim_info #type: SimInfo
        else:
            active_sim_info = sim_info_inp #type: SimInfo

        full_name = active_sim_info.first_name + " " + active_sim_info.last_name
        for sim_id in dialog.get_result_tags():
            sim_info_picked = services.sim_info_manager().get(sim_id) #type: SimInfo

            if sim_info_picked is not None:

                a = sim_info_picked.death_tracker  # type: DeathTracker
                if a.death_time is not None and a.death_time > 0:
                    sim_info_picked.trait_tracker.remove_traits_of_type(traits.TraitType.GHOST)
                    dt = sim_info_picked.death_tracker #type: DeathTracker
                    dt.clear_death_type()
                    sim_info_picked.update_age_callbacks()
                    sim = sim_info_picked.get_sim_instance(allow_hidden_flags=ALL_HIDDEN_REASONS) #type:Sim
                    if sim is not None:
                        sim.routing_context.ghost_route = False

                household_manager = services.household_manager() #type: HouseholdManager
                household_manager.switch_sim_household(sim_info_picked, active_sim_info)

                try:
                    SimSpawner.load_sim(sim_info_picked.sim_id)
                except Exception as e:
                    atmp="a"

            # c= "44444"
            # result = get_localized_string(object_value=0xA92F2705, tokens=(c, c), )
            # sim_impregnated_title = get_localized_string(object_value=0x9DB5F52C)
            # show_notification(result, title=c, sim_info=active_sim_info)

        #######################################

    localized_title = lambda **_: get_localized_string(object_value=0x8948B354)  # Select sim
    localized_text = lambda **_: get_localized_string(
        object_value=0x572C0443)  # Revive sim

    max_selectable_immutable = sims4.collections.make_immutable_slots_class(
        set(['multi_select', 'number_selectable', 'max_type']))
    max_selectable = max_selectable_immutable({'multi_select': False, 'number_selectable': 1, 'max_type': 1})

    client = services.client_manager().get_first_client()
    dialog = UiSimPicker.TunableFactory().default(client.active_sim, text=localized_text, title=localized_title,
                                                  max_selectable=max_selectable, min_selectable=1,
                                                  should_show_names=True,
                                                  hide_row_description=False)

    simInfoManager = services.sim_info_manager()  # type: SimInfoManager
    for sim_info in simInfoManager.get_all():  # type: SimInfo
        dialog.add_row(SimPickerRow(sim_info.sim_id, False, tag=sim_info.sim_id))
    dialog.add_listener(get_inputs_callback)
    dialog.show_dialog(icon_override=(None, sim_info))